import os.path
import sys, os
from time import gmtime, strftime
from myOpenseeAPI import *
from pprint import pprint


opensee = myOpenseeAPI(debug=False);

dt = datetime.datetime.now();
new_dt = str(dt.year)+'-'+str(dt.month)+'-'+str(dt.day)+'_'+str(dt.hour)+str(dt.minute)+str(dt.second);



print("\n== General ==");

print(">> General | Get information on Opensee version");
opensee.ping();

print(">> General | Provide credentials to get a token");
opensee.login('guillaume','Welcome?');

print(">> General | Get user information");
opensee.whoami();

print(">> General | Get modules names");
opensee.modules();

print(">> General | Get all module's elements")
opensee.moduleElements("HistoVaR");

print(">> General | Get module definition");
opensee.moduleDefinition("HistoVaR");

print(">> General | List pivots definitions of a module");
opensee.modulePivot("HistoVaR");

print(">> General | Get definition for a given pivot");
opensee.modulePivotDef("HistoVaR", "position_pivot");

print(">> General | Get all limits of a given module");
opensee.moduleLimit("HistoVaR");

print(">> General | Get a given limit of a given module");
opensee.moduleLimitDef("HistoVaR", "Portfolio");

print(">> General | Get the definition of all dimension groups of a given module");
opensee.moduleDimension("HistoVaR");

print(">> General | Get the definition of a given dimension group of a module");
opensee.moduleDimensionGroup("sensi", "FRTBGroupEQ");

print(">> General | Provide the full schema for a given table");
opensee.tablesSchema("Sensi");

print(">> General | Opensee settings for a given table");
opensee.tablesSettings("Sensi");

print(">> General | Opensee ingestion settings for a given table");
opensee.tableIngestion("Sensivity", "Sensi");



print("\n== Routes ==");

print(">> Routes | POST ​/beta​/module​/{module}​/pivot​/{pivot}​/dimension​/{dimension}");
opensee.betaModulePivotDimension('HistoVaR', 'position_PnL_pivot', 'Tenor');

print(">> Routes | DELETE ​/beta​/module​/{module}​/pivot​/{pivot}​/dimension​/{dimension}");
opensee.betaModulePivotDimensionDel('HistoVaR', 'position_PnL_pivot', 'Tenor');

jd = {
      "function": "3*Count()",
      "facets": [],
      "currency": None,
      "description" : " Counts ..."
    }
print(">> Routes | POST ​/beta​/module​/{module}​/pivot​/{pivot}​/metric​/{metric}");
opensee.betaModulePivotMetricPost('HistoVaR', 'position_PnL_pivot', 'Test', jd);

print(">> Routes | PUT ​/beta​/module​/{module}​/pivot​/{pivot}​/metric​/{metric}");
opensee.betaModulePivotMetricPut('HistoVaR', 'position_PnL_pivot', 'Test', jd);

print(">> Routes | DELETE ​/beta​/module​/{module}​/pivot​/{pivot}​/metric​/{metric}");
opensee.betaModulePivotMetricDel('HistoVaR', 'position_PnL_pivot', 'Test');


print("\n== Users ==");

print(">> User | create a user");
jd = {  "id": "test_login",
            "password": "test",
            "mandatoryFilters": {
                "@and": [{
                    "Country": {
                        "values": ["EU ZONE"],
                        "notValues": [],
                        "ranges": [],
                        "dateRanges": [],
                        "sql": '',
                        "match": ''
                    }
                }]
          },
          "rafaldbUser": "default"
}
opensee.userCreate(jd);

print(">> User | list all existing users");
opensee.userList();

print(">> User | get an user");
opensee.userId("test_login");

print(">> User | reset the password of an user");
opensee.userReset("test_login", "87t14ufjkdsahg9");

print(">> User | delete an user");
opensee.userDelete("test_login");

print(">> User | list all the groups of one user");
opensee.userListGroup("admin");



print("\n== Groups ==");

print(">> Group | list all the groups");
opensee.groupList();

print(">> Group | create a new empty group just for the right policy");
opensee.groupCreate("Group1");

print(">> Group | get one group");
opensee.groupGet("Group1");

print(">> Group | create or update a database user group");
opensee.groupDbUser("Group2", "test1");
opensee.groupDbUser("Group2", "test2");

print(">> Group | create or update a mandatory filter user group");
jd = {
        "liquidity": {
        "Date": { "values": ["2019-04-15"] }
    }
}
opensee.groupfilter("Group3", jd);

print(">> Group | add a policy to a group.");
jd = {
      "id": "9de78f13-bc16-4bbb-abcd-8b5db16c4ff4",
      "actions": [
        "runCalculator",
        "editCalculator"
      ],
      "resource": "calculator:*"
}
opensee.groupPolicyadd("Group1", jd);

print(">> Group | remove a policy from a group");
jd = {
      "id": "9de78f13-bc16-4bbb-abcd-8b5db16c4ff4",
      "actions": [],
      "resource": "calculator:*"
}
opensee.groupPolicydel("Group1", jd);

print(">> Group | delete a group");
opensee.groupDelete("Group1");
opensee.groupDelete("Group2");
opensee.groupDelete("Group3");

print(">> Group | list all users in a group");
opensee.groupUserList("Admin");

print(">> Group | add a user to a group");
opensee.groupUseradd("Admin", "test");

print(">> Group | add a user to multiple groups");
opensee.groupUseraddmult(["Admin", "AutoCube"], "test");

print(">> Group | remove a user from a group");
opensee.groupUserremove("Admin", "test");



print("\n== Data Accessibility ==");

print(">> Data | Get values of all enumerable dimensions");
opensee.dataGetdim("HistoVaR");

print(">> Data | Get values of a given dimension");
opensee.dataGetdimValue("HistoVaR", "Tenor");

print(">> Data | Smart filter on a given dimension");
jd= {"where": {
        "where": {
            "@and": [{
                "Date": { "values": ["2019-04-15"] }
            },{
                "RiskType": { "values": [], "notValues": ["gamma"] }
            }]
        }
      },
      "dimension": "MaturityDate",
      "pivot": "delta"
    }
opensee.dataGetdimFilter("sensi", jd);

print(">> Data | Query a pivot");
jd= {
    "metrics": [
        "Sensi"
    ],
    "postaggregaggr": {
        
    },
    "with_facet": False,
    "batch": False,
    "pivot": "security_sensi_pivot",
    "by": [
        "CountryId",
        "Ccy"
    ],
    "where": {
        "@and": [
            
        ]
    },
    "versioning": [
        
    ],
    "metaData": {
        "module": "HistoVaR",
        "rows": [
            "CountryId"
        ],
        "documentName": "Document_0",
        "type": "P"
    }
}
opensee.dataQueryPivot("HistoVaR", "2449be20-cba8-49f1-b6f6-1a649dad189b", jd);

print(">> Data | Query most granular data");
jd = {
    "columns":[
			"Date"
		],
    "pivot": "MTM",
    "by": [
        
    ],
    "where": {
        
    },
    "versioning": [
        {
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
            "asOf": "2019-04-15",
            "comment": "Initial data for the day"
        },
        {
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
            "asOf": "2019-04-16",
            "comment": "Initial data for the day"
        }
    ]
}
opensee.dataGetDetails("mtm", jd);

print(">> Data | Get all calculator definitions for a given module");
opensee.dataGetCalculator("sensi");

print(">> Data | Create or update a given calculator in a given module");
jd = jd = {'module': 'sensi', 'name': 'test', 'metrics': [], 'dimensions': [{'name': 'Branch', 'description': None, 'optional': False}, {'name': 'Book', 'description': None, 'optional': False}, {'name': 'BusinessLine', 'description': None, 'optional': False}], 'code': '', 'parameters': '', 'requests': [{'name': 'Request0', 'functions': ['Sum(Delta)'], 'table': 'Sensi', 'by': ['Country', 'Csa'], 'where': {'@and': []}, 'facets': [], 'runTimeGroupBy': 'Append', 'runTimeFilter': 'Intersection', 'distribute': False, 'orderBy': []}], 'defaults': {'dimensions': ['Book', 'Branch', 'BusinessLine'], 'where': {'@and': []}, 'cols': [], 'rows': []}, 'distribution': {'distributionAxis': None, 'chunkCount': 0, 'parallelism': 0, 'filteringMethod': 'RequireUniformity'}}
opensee.dataCreateCalculator("sensi", "test", jd);

print(">> Data | Get the definition of a given calculator for a given module");
opensee.dataGetCalculatorDef("sensi", "test");

print(">> Data | Get personalized drill functions for a given calculator in a given module");
opensee.dataGetCalculatorUserdrill("sensi", "test");

print(">> Data | Delete a given calculator in a given module");
jd = {"hardDelete": True}
opensee.dataDelCalculator("sensi", "Test", jd);

print(">> Data | Run a Python calculator");
jd = {
    "forceNonDistributed": False,
    "batch_description": None,
    "by": [
        "Portfolio"
    ],
    "where": {
        
    },
    "calculatorName": "Projection",
    "parameters": {
        "mapping": {
            "1Y": {
                "7M": 0.17,
                "12M": 1,
                "11M": 0.83,
                "8M": 0.33,
                "10M": 0.67,
                "9M": 0.5
            },
            "N/A": {
                "N/A": 1
            },
            "10Y": {
                "16Y": 0.7,
                "26Y": 0.2,
                "10Y": 1,
                "22Y": 0.4,
                "27Y": 0.15,
                "7Y": 0.4,
                "9Y": 0.8,
                "20Y": 0.5,
                "8Y": 0.6,
                "15Y": 0.75,
                "19Y": 0.55,
                "28Y": 0.1,
                "25Y": 0.25,
                "24Y": 0.3,
                "21Y": 0.45,
                "6Y": 0.2,
                "18Y": 0.6,
                "14Y": 0.8,
                "12Y": 0.9,
                "29Y": 0.05,
                "23Y": 0.35,
                "17Y": 0.65,
                "13Y": 0.85,
                "11Y": 0.95
            },
            "6M": {
                "1M": 1,
                "7M": 0.83,
                "O/N": 1,
                "6M": 1,
                "3W": 1,
                "11M": 0.17,
                "5M": 1,
                "2M": 1,
                "8M": 0.67,
                "2W": 1,
                "3M": 1,
                "10M": 0.33,
                "9M": 0.5,
                "4M": 1,
                "T/N": 1,
                "1W": 1
            },
            "50Y": {
                "35Y": 0.25,
                "32Y": 0.1,
                "50Y": 1,
                "45Y": 0.75,
                "31Y": 0.05,
                "40Y": 0.5
            },
            "5Y": {
                "7Y": 0.6,
                "9Y": 0.2,
                "8Y": 0.4,
                "5Y": 1,
                "4Y": 0.5,
                "6Y": 0.8,
                "3Y": 0.5
            },
            "30Y": {
                "16Y": 0.3,
                "26Y": 0.8,
                "35Y": 0.75,
                "32Y": 0.9,
                "22Y": 0.6,
                "27Y": 0.85,
                "20Y": 0.5,
                "45Y": 0.25,
                "15Y": 0.25,
                "19Y": 0.45,
                "28Y": 0.9,
                "25Y": 0.75,
                "30Y": 1,
                "24Y": 0.7,
                "21Y": 0.55,
                "18Y": 0.4,
                "14Y": 0.2,
                "31Y": 0.95,
                "12Y": 0.1,
                "29Y": 0.95,
                "23Y": 0.65,
                "17Y": 0.35,
                "40Y": 0.5,
                "13Y": 0.15,
                "11Y": 0.05
            },
            "2Y": {
                "2Y": 1,
                "3Y": 0.5,
                "4Y": 0.5
            }
        },
        "col_name": "Tenor",
        "aggregation_col": "result",
        "pivot_fields": {
            "rows": [
                "Portfolio"
            ],
            "columns": [
                
            ]
        },
        "metrics": [
            "result"
        ]
    },
    "versioning": {
        "Position_Sensi_WIP": [
            {
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
                "asOf": "2020-04-01",
                "comment": "Initial data for the day"
            },
            {
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
                "asOf": "2020-04-02",
                "comment": "Initial data for the day"
            }
        ]
    }
}
opensee.dataRunPython("HistoVaR", jd);

print(">> Data | Test the calculator on the server without saving it");
jd = {
    "calculator": {
        "module": "sensi",
        "name": "FRTB_GIRR_Sensi",
        "requests": [
            {
                "name": "sum(Delta) request",
                "functions": [
                    "sum(Delta)"
                ],
                "table": "Sensi",
                "by": [
                    "RiskName",
                    "Tenor",
                    "Currency"
                ],
                "where": {
                    "@and": [
                        {
                            "RiskClass": {
                                "values": [
                                    "ir"
                                ]
                            }
                        },
                        {
                            "RiskType": {
                                "values": [
                                    "delta"
                                ]
                            }
                        },
                        {
                            "Date": {
                                "values": [
                                    "2019-04-15"
                                ]
                            }
                        }
                    ]
                },
                "facets": [
                    "Sensi_by_treeprod"
                ],
                "runTimeGroupBy": "Append",
                "runTimeFilter": "Intersection",
                "distribute": False,
                "orderBy": []
            }
        ],
        "defaults": {
            "dimensions": [
                "BusinessLine",
                "Desk",
                "Book",
                "Date"
            ],
            "where": {
                "Book": {
                    "values": [
                        "Book0000"
                    ]
                }
            },
            "cols": [
                "BusinessLine"
            ],
            "rows": []
        },
        "distribution": {
            "distributionAxis": None,
            "chunkCount": 1,
            "parallelism": 1,
            "filteringMethod": "RequireUniformity"
        },
        "code": "def calculate(input_dataframes, parameters_dict):\n\timport pandas as pd\n\timport numpy as np\n\timport math\n\n\n\twir = parameters_dict[\"wir\"]\n\tbuckets = parameters_dict[\"buckets\"]\n\trbucket = {k: v for v, vs in buckets.items() for k in vs}\n\n\n\tnpwir = np.array([wir[k] for k in sorted(wir)])\n\n\tbigT = {\n\t\t\"M03\": 0.25,\n\t\t\"M06\": 0.5,\n\t\t\"Y01\": 1.0,\n\t\t\"Y02\": 2.0,\n\t\t\"Y03\": 3.0,\n\t\t\"Y05\": 5.0,\n\t\t\"Y10\": 10.0,\n\t\t\"Y15\": 15.0,\n\t\t\"Y20\": 20.0,\n\t\t\"Y30\": 30.0\n\t}\n\n\trho_irm_at = 0.999\n\n\tgamma_ir = 0.5\n\n\tdef rho(Currency, c1, t1, c2, t2):\n\t\tif c1 == c2 and t1 == t2:\n\t\t\treturn 0\n\t\tif c1.startswith('Xccy') or c2.startswith('Xccy'):\n\t\t\treturn 0.0\n\t\tif c1.startswith('Infla') or c2.startswith('Infla'):\n\t\t\treturn 0.4\n\t\tif c1 != c2 and t1 == t2:\n\t\t\treturn rho_irm_at\n\t\tif c1 == c2 and t1 != t2:\n\t\t\treturn max(math.exp(-0.03*abs(bigT[t1]-bigT[t2])/min(bigT[t1], bigT[t2])), 0.4)\n\t\tif c1 != c2 and t1 != t2:\n\t\t\treturn rho_irm_at*max(math.exp(-0.03*abs(bigT[t1]-bigT[t2])/min(bigT[t1], bigT[t2])), 0.4)\n\n\n\tdef ir_delta_kb(Currency, sensi, rho_matrices):\n\t\ts = 0.0\n\t\tby_curb = {\n\t\t\tc: g.set_index(\"Tenor\").reindex(sorted(buckets.keys()), fill_value=0.0).sort_index(ascending=True).result\n\t\t\tfor c, g in sensi.groupby(\"RiskName\")\n\t\t}\n\t\tws = {c: npwir*g for c, g in by_curb.items()}\n\t\tfor c in by_curb:\n\t\t\ts += np.dot(ws[c], ws[c].T)\n\t\t\tfor c2 in by_curb:\n\t\t\t\ts += np.dot(ws[c], np.dot(rho_matrices[(Currency, c, c2)], ws[c2]))\n\t\tif s > 0:\n\t\t\treturn math.sqrt(s)\n\t\treturn 0\n\n\n\tdef ir_sws_b(sensi):\n\t\ts = 0.0\n\t\tby_curb = {\n\t\t\tc: g.set_index(\"Tenor\").reindex(sorted(buckets.keys()), fill_value=0.0).sort_index(ascending=True).result\n\t\t\tfor c, g in sensi.groupby(\"RiskName\")\n\t\t}\n\t\tws = {c: npwir*g for c, g in by_curb.items()}\n\t\tfor c in by_curb:\n\t\t\ts+= np.dot(ws[c],np.ones(len(ws[c])).T)\n\t\treturn s\n\n\n\tdef ir_delta(sws_b, delta_kb):\n\t\tsquare = 0.0\n\t\trhom = 0.0\n\t\tfor c in delta_kb:\n\t\t\tsquare += delta_kb[c]*delta_kb[c]\n\t\t\tfor c2 in delta_kb:\n\t\t\t\tif c2 != c :\n\t\t\t\t\trhom += gamma_ir*sws_b[c]*sws_b[c2]\n\t\trr = square + rhom\n\t\tif rr < 0.0:\n\t\t\trr -= rhom \n\t\t\trhom = 0.0\n\t\t\tsws_b_alt = {c:max(min(sws_b[c],delta_kb[c]),-delta_kb[c])\n\t\t\t\t\t\t for c in delta_kb\n\t\t\t\t\t\t}\n\t\t\tfor c in delta_kb:\n\t\t\t\tfor c2 in delta_kb:\n\t\t\t\t\tif c2 != c :\n\t\t\t\t\t\trhom += gamma_ir*sws_b_alt[c]*sws_b_alt[c2]\n\t\t\trr += rhom\n\t\treturn np.sqrt(rr)\n\n\n\tsensibilities = input_dataframes[0]\n\n\tcurbs_by_currencies = {c: list(sensibilities[sensibilities.Currency == c][\"RiskName\"].unique()) for c in sensibilities[\"Currency\"].unique()}\n\tsensibilities[\"Tenor\"] = sensibilities[\"Tenor\"].replace(rbucket)\n\tsensibilities = sensibilities.groupby([c for c in sensibilities.columns if c != \"result\"]).sum().reset_index()\n\n\trho_matrices = {\n\t\t(b, c1, c2): np.array([\n\t\t\t[rho(b, c1, t1, c2, t2) for t1 in sorted(wir)]\n\t\t\tfor t2 in sorted(wir)])\n\t\tfor b in curbs_by_currencies\n\t\tfor c1 in curbs_by_currencies[b]\n\t\tfor c2 in curbs_by_currencies[b]\n\t}\n\n\tby = [c for c in sensibilities.columns if c not in [\"result\", \"Tenor\", \"Currency\", \"RiskName\"]]\n\n\tres = []\n\tfor by_values, group1 in sensibilities.groupby(by) if by else [(None, sensibilities)]:\n\t\tif len(by) == 0:\n\t\t\tline = {}\n\t\telif len(by) == 1:\n\t\t\tline = {by[0]: by_values}\n\t\telse:\n\t\t\tline = dict(zip(by, by_values))\n\t\tby_Currency = {c: ir_delta_kb(c, group2, rho_matrices) for c, group2 in group1.groupby(\"Currency\")}\n\t\tSWS = {c : ir_sws_b(group2) for c, group2 in group1.groupby(\"Currency\")}\n\t\tfor c in by_Currency:\n\t\t\tline[\"Currency\"] = c\n\t\t\tline['result'] = by_Currency[c] * 100\n\t\t\tres.append(line.copy())\n\t\tline[\"Currency\"] = \"_Total\"\n\t\tline['result'] = ir_delta(SWS, by_Currency) * 100\n\t\tres.append(line.copy())\n\n\treturn pd.DataFrame(res)",
        "parameters": {
            "wir": {
                "M06": 0.024,
                "Y15": 0.015,
                "Y01": 0.0225,
                "Y30": 0.015,
                "Y03": 0.0173,
                "M03": 0.024,
                "Y20": 0.015,
                "Y02": 0.0188,
                "Y05": 0.15,
                "Y10": 0.15
            },
            "buckets": {
                "M06": [
                    "M05",
                    "M06",
                    "M07",
                    "M08",
                    "M09",
                    "M10"
                ],
                "Y15": [
                    "Y13",
                    "Y14",
                    "Y15",
                    "Y16",
                    "Y17"
                ],
                "Y01": [
                    "M11",
                    "1Y"
                ],
                "Y30": [
                    "Y26",
                    "Y27",
                    "Y28",
                    "Y29",
                    "Y30",
                    "Y31",
                    "Y32",
                    "Y33",
                    "Y34",
                    "Y35",
                    "Y40",
                    "Y45",
                    "Y50"
                ],
                "Y03": [
                    "Y03",
                    "Y04"
                ],
                "M03": [
                    "D02",
                    "M01",
                    "M02",
                    "M03",
                    "M04"
                ],
                "Y20": [
                    "Y18",
                    "Y19",
                    "Y20",
                    "Y21",
                    "Y22",
                    "Y23",
                    "Y24",
                    "Y25"
                ],
                "Y02": [
                    "Y02"
                ],
                "Y05": [
                    "Y05",
                    "Y06",
                    "Y07"
                ],
                "Y10": [
                    "Y08",
                    "Y09",
                    "Y10",
                    "Y11",
                    "Y12"
                ]
            }
        }
    },
    "versioning": {
        "Sensi": [
            {
                "asOf": "2019-04-11",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            },
            {
                "asOf": "2019-04-12",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            },
            {
                "asOf": "2019-04-13",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            },
            {
                "asOf": "2019-04-14",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            },
            {
                "asOf": "2019-04-15",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            },
            {
                "asOf": "2019-04-16",
                "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
            }
        ]
    }
}
opensee.dataTestCalculator("sensi", jd);

print(">> Data | Run a given calculator request");
jd = {
    "rawRequest": {
        "name": "sum(Delta) request",
        "functions": [
            "sum(Delta)"
        ],
        "table": "Sensi",
        "by": [
            "RiskName",
            "Tenor",
            "Currency"
        ],
        "where": {
            "@and": [
                {
                    "RiskClass": {
                        "values": [
                            "ir"
                        ]
                    }
                },
                {
                    "RiskType": {
                        "values": [
                            "delta"
                        ]
                    }
                },
                {
                    "Date": {
                        "values": [
                            "2019-04-15"
                        ]
                    }
                }
            ]
        },
        "facets": [
            "Sensi_by_treeprod"
        ],
        "runTimeGroupBy": "Append",
        "runTimeFilter": "Intersection",
        "distribute": False,
        "orderBy": []
    },
    "versioning": [
        {
            "asOf": "2019-04-11",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        },
        {
            "asOf": "2019-04-12",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        },
        {
            "asOf": "2019-04-13",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        },
        {
            "asOf": "2019-04-14",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        },
        {
            "asOf": "2019-04-15",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        },
        {
            "asOf": "2019-04-16",
            "commit": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        }
    ],
    "defaults": {
        "dimensions": [
            "BusinessLine",
            "Desk",
            "Book",
            "Date"
        ],
        "where": {
            "Book": {
                "values": [
                    "Book0000"
                ]
            }
        },
        "cols": [
            "BusinessLine"
        ],
        "rows": []
    }
}
res = opensee.dataCalculatorRequest("sensi_dynamic", jd);



print("\n== Versioning ==");

print(">> Versioning | Create a commit (update, insert or light delete)");
jd = {
    "source": "Manual",
    "comment": new_dt,
    "parent1": "e8d1a70ce19d24d4ddcb30bdf7077e85d864e211c0ff78bf050e8dba4d720966",
    "asOf": "2019-04-15",
    "upsert": {
        "data": [
            {
                "Book": "Book0000",
                "Branch": "Branch02",
                "BusinessLine": "BusLine0",
                "CounterParty": "Counterparty219",
                "Country": "AUSTRALIA",
                "Cpty4MTM": "Cpty219",
                "Csa": "Csa877",
                "CsaId": "877",
                "Currency": "AUD",
                "CurrencyGroup": "XMAJOR",
                "CurrencyGroup2": "G22",
                "CurrentNominal": 9.804963E+07,
                "CurveTenor": "D1",
                "Date": "2019-04-15T00:00:00",
                "Delta": 999999.0,
                "Desk": "Desk00",
                "Entity": "Entity01",
                "Expiry": "",
                "ExpiryGroup2": "others",
                "FRTBGroupEQ2": "others",
                "FoSystem": "Summit",
                "Folder": "Folder01",
                "GeoZone": "Nikkei225",
                "InstType": "SWAP",
                "MaturityDate": "2045-04-17T00:00:00",
                "NettingSet": "NettingSet438",
                "NominalCurrency": "AUD",
                "OrigNominal": 378803168.0,
                "ProductClass": "IRLinear",
                "ProductCode": "FRA",
                "RfUnderName": "OIS",
                "RiskClass": "ir",
                "RiskName": "irAUDOIS",
                "RiskType": "delta",
                "SensId": 17793,
                "SensitivityId": 17793,
                "Tenor": "M07",
                "TenorGroup": "Y00-Y02",
                "TradeDate": "2011-09-18T00:00:00",
                "TradeId": 860,
                "TradeUnderName": "AUD"
            }
        ]
    }
}
opensee.versionCommit("Sensi", jd);

print(">> Versioning | Provide the schema of a given table");
opensee.versionCommitDef('Sensi');

print(">> Versioning | Get all commits for a given Date");
opensee.versionCommitDate("Sensi","2020-02-19");

print(">> Versioning | Get all ancestor commits of a given commit on a given date");
opensee.versionAncestorCommitDate("Sensi", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", "2019-04-15");

print(">> Versioning | Get branches for the requested table");
opensee.versionBranches("Sensi");

print(">> Versioning | Get commits of a branch for a given date");
opensee.versionCommitDefDate("Sensi", "official", "2019-04-11");

print(">> Versioning | Change a comment for given commit");
jd = {"comment": "my new comment"}
opensee.versionCommitComment("Sensi", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", "2019-04-11", jd);




print("\n== Data ingestion ==");

print(">> Ingest | Delete a specific commit");
jd = {"dryRun": True, "resetCommit": ""};
opensee.ingestionDelCommit("Sensi", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b966", "2020-02-19", jd)

# print(">> Ingest | Insert data in a dictionary, generating a private key for each row.");
# opensee.dataInsert("2020-03-20", "CsaId", {});

print(">> Ingest | Start a new transaction");
opensee.autocubeCreate("module_test", "autocube_input.json", "Create");
transaction_id1 = opensee.ingestionTransactionOpen("Facttable", '2021-05-07', 'official', 'ingestion_fact_test_0');
transaction_id2 = opensee.ingestionTransactionOpen("Facttable", '2021-05-07', 'official', 'ingestion_fact_test_0');

print(">> Ingest | ingest fact table");
opensee.ingestionFactTable("Facttable", transaction_id1, '2021-05-07', 'ingestion_fact_test_0.csv');

print(">> Ingest | rollback an open transaction, which deletes the committed data");
opensee.ingestionRollback("Facttable", transaction_id2, '2021-05-07');

print(">> Ingest | Close an open transaction");
opensee.ingestionTransactionClose("Facttable", transaction_id1, '2021-05-07', 'official');

print(">> Ingest | Retrieve the number of rows inserted");
opensee.ingestionRetriew("Facttable", "2021-05-07", transaction_id1);



print("\n== Configuration ==");

print(">> Config | Update a module definition");
jd = {'module': 'mtm', 'tables': ['mtms', 'fxs', 'diffusions', 'fxRate_sync'], 'facts': ['mtms', 'cdsSpread', 'rate', 'diffusions', 'fxs'], 'dimensions': {'nettingsetId': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'originalNominal': {'enumerable': False, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'SensId': {'enumerable': False, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'Country': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'book': {'enumerable': True, 'folder': 'Portfolio', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'counterpartyId': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'to': {'enumerable': True, 'folder': 'FxRate', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'CurveTenor': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'dates': {'enumerable': False, 'folder': 'Base', 'type': 'Date', 'anonymize': False, 'description': '', 'alias': ''}, 'Date': {'enumerable': True, 'folder': 'Base', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'Expiry': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'currentNominal': {'enumerable': True, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'productCode': {'enumerable': False, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'scenario': {'enumerable': True, 'folder': 'Base', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'RiskClass': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'RfUnderName': {'enumerable': True, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'RiskType': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'counterparty': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'cdsSpread': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'businessLine': {'enumerable': True, 'folder': 'Portfolio', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'lag': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'Tenor': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'nettingset': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'industry': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'csaCcy': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'trade': {'enumerable': True, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'country': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'csa': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'desk': {'enumerable': True, 'folder': 'Portfolio', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'Currency': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'RiskName': {'enumerable': True, 'folder': 'Risk', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'threshold': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'from': {'enumerable': True, 'folder': 'FxRate', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'csaId': {'enumerable': False, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'counterpartyCcy': {'enumerable': True, 'folder': 'Credit', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}, 'valuationCcy': {'enumerable': True, 'folder': 'Trade', 'type': 'Default', 'anonymize': False, 'description': '', 'alias': ''}}, 'dictionaries': {'originalNominal': 'trade', 'Country': 'SensId', 'book': 'trade', 'country': 'csaId', 'to': 'fxRate', 'CurveTenor': 'SensId', 'InstType': 'SensId', 'Expiry': 'SensId', 'currentNominal': 'trade', 'productCode': 'trade', 'rate': 'fxRate', 'RiskClass': 'SensId', 'RfUnderName': 'SensId', 'RiskType': 'SensId', 'counterparty': 'counterpartyId', 'cdsSpread': 'csaId', 'businessLine': 'trade', 'lag': 'csaId', 'Tenor': 'SensId', 'nettingset': 'nettingsetId', 'industry': 'csaId', 'csaCcy': 'csaId', 'csa': 'csaId', 'desk': 'trade', 'Currency': 'SensId', 'RiskName': 'SensId', 'threshold': 'csaId', 'from': 'fxRate', 'counterpartyCcy': 'counterpartyId', 'valuationCcy': 'trade'}, 'facets': ['mtm_facet_csa'], 'arrays': [{'factTable': 'mtms', 'label': 'mtms', 'zipTable': 'datearray', 'zipLabel': 'dates', 'joinKeys': None}, {'factTable': 'fxs', 'label': 'fxs', 'zipTable': 'datearray', 'zipLabel': 'dates', 'joinKeys': None}, {'factTable': 'diffusions', 'label': 'diffusions', 'zipTable': 'datearray', 'zipLabel': 'dates', 'joinKeys': None}], 'arraysDimensions': [], 'settings': {'valuesLimit': 15000}}
opensee.configDefPost('mtm', jd);

print(">> Config | Update pivot of a given module");
jd = {'module': 'HistoVaR', 'name': 'position_pivot', 'metrics': {'quantity': {'function': 'sum(Units)', 'facets': [], 'currency': None, 'description': None}, 'count': {'function': 'count(*)', 'facets': [], 'currency': None, 'description': None}, 'sumUserFlagFloat': {'function': 'sum(UserFlagFloat)', 'facets': [], 'currency': None, 'description': None}}, 'defaultMetric': None, 'rows': ['Position', 'Security'], 'cols': ['Date'], 'dimensions': ['Date', 'PositionId', 'Position', 'MicroStrategy', 'MicroStrategyGroup', 'Account', 'PortfolioId', 'Portfolio', 'PortfolioScope', 'Supervisor', 'PrimaryStrategy', 'Sector', 'SecurityId', 'Security', 'CountryId', 'CurrencyId', 'SecurityTypeId', 'BBDescription', 'BBKey', 'BlmSecurityType', 'BloombergCode', 'BloombergTicker', 'Cusip', 'Description', 'Seniority', 'Isin', 'Sedol', 'UReutersTicker', 'WKN', 'IndustrySector', 'IndustryGroup', 'GicsSector', 'GicsIndustryGroup', 'GicsIndustry', 'GicsSubIndustry', 'IcbIndustry', 'IcbSuperSector', 'IcbSector', 'IcbSubSector', 'UserFlagString', 'UserFlagInt', 'CountryNOrm'], 'facts': ['Units', 'UserFlagFloat'], 'table': 'Position_WIP', 'where': {'@and': []}, 'postaggregkey': [''], 'postaggregations': {}}
opensee.configPivotPost('HistoVaR', 'position_pivot', jd);

print(">> Config | Create or update a limit of a module");
jd = {'PF241': -2000000.0, 'PF242': -2000000.0}
opensee.configLimitPost("HistoVaR", "ProfitLoss", jd);

print(">> Config | Delete a limit in a given module");
jd = {"hardDelete": True}
opensee.configLimitDelete("HistoVaR", "ProfitLoss", jd);

print(">> Config | Delete ingestion settings for a given table");
jd = {
    "hardDelete": True
};
opensee.configIngestDel("sensi", "Sensi", jd);

print(">> Config | Insert ingestion settings for a given table");
jd = {'dictionaries': [],
 'facettes': [{'dimensions': ['Date',
                              'TreeId',
                              'ProductId',
                              'SensId',
                              'SensitivityId'],
               'name': 'Sensi_by_treeprod'}],
 'facts': ['Delta'],
 'insertMethod': 'ForkAtLastBlock',
 'insertQuery': 'INSERT INTO Sensi SELECT  ~~as_of~~  Date,TradeId, '
                'SensitivityId, SensId, ProductId, TreeId, CsaId, '
                'MaturityDate, TradeDate, NominalCurrency, OrigNominal, '
                'CurrentNominal, TradeUnderName, Delta , ~~commit~~, 1, 1 FROM '
                "input('TradeId UInt64 , SensitivityId UInt64, SensId UInt64, "
                'ProductId UInt64, TreeId UInt64, CsaId UInt64, MaturityDate '
                'Date, TradeDate Date, NominalCurrency String, OrigNominal '
                'Float32, CurrentNominal Float32, TradeUnderName String,Delta '
                "Float32')",
 'protobufSchema': 'SensiSchema',
 'table': {'database': 'default', 'table': 'Sensi'},
 'tableRead': None,
 'tableWrite': None}
opensee.configIngestPost("sensi", "Sensi", jd);

print(">> Config | Add a dimension to a module");
jd = {
  "addColumn": "decorationName2",
  "dataType": "String",
  "defaultValue": "undefined"
}
opensee.configDimPost("sensi", "Sensi", jd);

print(">> Config | Delete a dimension from a module");
jd = {
  "deleteColumn": "decorationName2"
}
opensee.configDimDel("sensi", "Sensi", jd);

print(">> Config | Add a dimension to a module (2)");
jd = {
  "addColumn": "decorationName",
  "dataType": "String",
  "defaultValue": "undefined"
}
opensee.configDimPost2("sensi", "default", "Sensi", jd);

print(">> Config | Delete a dimension from a module (2)");
jd = {
  "deleteColumn": "decorationName"
}
opensee.configDimDel2("sensi", "default", "Sensi", jd);



print("\n== Dot Net Configuration ==");

print(">> .NET | Info");
opensee.dnetInfo('Layout');

print(">> .NET | Update or create a dot net configuration");
jd = {  "type": "Layout",
        "shared": False,
        "name": "SensitivityLayoutName",
        "tag": '',
        "userId": "guillaume",
        "value": [
            "string"
        ]
}
opensee.dnetCreate(jd);

print(">> .NET | Delete a dot net configuration for a given type and a given object");
opensee.dnetDel("SensitivityLayoutName", "Layout");



print("\n== AbstractionLayer ==");

print("AbstractionLayer | Get informations about joins available for this module");
opensee.alGetJoin("sensi");

print("AbstractionLayer | meta is a new endpoint which will be used by users or frontends to get all the needed information to then perform the request through the abstraction API.");
opensee.alMeta("sensi");

print("AbstractionLayer | Retrieve all available branches (and its dates) of a module");
opensee.alBranches("sensi");

print("AbstractionLayer | This endpoint is to get a list of pivot, that will then be able to use to run the existing endpoints smart filters (or values).");
jd = {  "dimension": "Book",
        "by": [
            "Book",
            "RiskType"
        ]
}
opensee.alPivot("sensi", jd);

# print("AbstractionLayer | This endpoint is to get a list of pivot, that will then be able to use to run the existing endpoints smart filters (or values).");
# jd = {
	# "dimension": "Date",
	# "by": [
		# "TradeId",
		# "Desk",
		# "Book",
		# "TenorGroup",
		# "Currency"
	# ]
# }
# opensee.alPivotSource("sensi", jd);

print("AbstractionLayer | This endpoint allows executing a source (pivot or calculator or join) with at least given dimensions, metrics and filter.");
jd = {
    "batch": False,
    "with_facet": True,
    "metrics": [
       "FRTB_GIRR"
    ],
    "by": [
        "Desk"
    ],
    "where": {
                    "Desk": {
            "values": [
                "Desk45"
            ]
        }
    },
    "branch": "official"
}
opensee.alRequest("sensi_dynamic", jd);

print("AbstractionLayer | This endpoint explains the execution of /request endpoint with a input. AbstractionLayer will execute the first correspondent source which matches all requirements (dimensions, metrics)");
jd = {
    "batch": False,
    "with_facet": True,
    "currency": None,
    "batch_description": None,
    "metrics": [
       "FRTB_GIRR"
    ],
    "by": [
        "Desk"
    ],
    "where": {
                    "Desk": {
            "values": [
                "Desk45"
            ]
        }
    },
    "branch": "official",
    "versioning": None
}
opensee.alExplain("sensi_dynamic", jd);



print("\n== Cache ==");

print("Cache | Reload a specific dictionary");
opensee.cacheReload("CsaId");



print("\n== Batch ==");

print(">> batch | status");
opensee.batchStatus('Layout');

print(">> batch | Removes old stored batch data");
jd = {'date':"2020-12-08", 'lostUUID':True}
opensee.batchClean(jd);

print(">> batch | Removes only one batch data");
opensee.batchDel("d19fd683-f780-463d-96cb-12836f503252");



print("\n== AutoCube ==");

print(">> autocube | Creates cube");
opensee.autocubeCreate("module_test");



print("\n== DataModel ==");

print(">> DataModel | ");




print("\n== Adjustment validation workflows ==");

print(">> Adj Validat° workflow | Adds a new validationflow");
jd = {
    "name": new_dt,
    "states": {
        "state1": {
            "approverGroups" : ["Admin"],
            "transition":"state2"
        },
        "state2": {
            "autoAPI": { "method": "GET", "url": "http://worldtimeapi.org/api/timezone/Europe/Paris", "body": "" },
            "approverGroups" : ["Admin"],
            "transition":"state3"
        },
        "state3": {
            "approverGroups" : ["Admin"],
            "transition":"state4"
        },	
        "state4": {
            "approverGroups" : ["Admin"]
        }
    },
    "description": "this is a descprition of the template5"
}
opensee.workflowNew(jd);

print(">> Adj Validat° workflow | Lists all validationflows");
opensee.workflowList();

print(">> Adj Validat° workflow | Retrievs an existing validationflow");
opensee.workflowRetriev(new_dt);


print(">> Adj Validat° workflow | Create a new validation request based on a validation flow.");
jd = {
    "workflowTemplate": new_dt,
    "table": "mtms",
    "source": "official",
    "destination": "official",
    "asOf": "2019-04-15",
    "conflictResolution": "incoming",
    "name": "instance2",
    "comment": "comment",
    "variables": {}
}
res = opensee.workflowCreateValidation(jd);

if (isinstance(res, str)):

    print(">> Adj Validat° workflow | Approve one step of a validation request");
    opensee.workflowAccept(res['id'], "state1");

    print(">> Adj Validat° workflow | Reject one step of a validation request");
    opensee.workflowReject(res['id'], "state3");

    print(">> Adj Validat° workflow | Retry the last step of a validation request");
    opensee.workflowRetry(res['id'], "state2");

    # print(">> Adj Validat° workflow | Manually override an automatic step of a validation request");
    # opensee.workflowOverride(res['id'], "state1");

    print(">> Adj Validat° workflow | Close a validation request");
    opensee.workflowClose(res['id']);

    print(">> Adj Validat° workflow | Reopen a closed validation request");
    opensee.workflowReopen(res['id']);

    print(">> Adj Validat° workflow | Get detail of validation request (name, table, states,...)");
    opensee.workflowDetails(res['id']);