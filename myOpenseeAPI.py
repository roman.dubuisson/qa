import json;
import requests;
# from datetime import datetime
import datetime
from datetime import timedelta
from requests.packages.urllib3.exceptions import InsecureRequestWarning;
requests.packages.urllib3.disable_warnings(InsecureRequestWarning);
from pprint import pprint;

import os;
from time import gmtime, strftime

import re


class RaiseError(Exception):
    pass;

class colors:
    Reset='\033[0m';
    LightRed='\033[91m';
    LightGreen='\033[92m';
    LightYellow='\033[93m';
    LightBlue='\033[94m';
    LightMagenta='\033[95m';
    LightCyan='\033[96m';
    Green='\033[32m';
    Gray='\033[90m';

        
class myOpenseeAPI:
        
    def __init__(self,debug=False):
        # self._uri = 'http://localhost:8081/';
        # self._uri = 'http://dev.opensee.ninja:8080/';
        # self._uri = 'http://dev58.opensee.ninja:9999/';
        # self._uri = 'http://review_release-5-10-x.opensee.ninja:8080/';
        # self._uri = 'http://review_release-5-9-x.opensee.ninja:8080/';
        self._uri = 'http://review_master.opensee.ninja:8080/';
        # self._uri = 'http://review_1284-fix-dimension-values-caption.opensee.ninja:8080/';
        # self._uri = 'http://devtest.opensee.ninja:9999/';
        # self._uri = 'https://devtest.opensee.ninja:9999/';
        # self._uri = 'http://review_release-5-11-x.opensee.ninja:8080/';
        self._token = None;
        # self._Debug = debug;

        
    def callAPI(self, endpoint, method, contentType="text/plain", jsonData=None, payload=None, retry=1, fullreponse=False):
        url = self._uri + endpoint;
        for i in range(0,retry):
            headers = {
                'cache-control': "no-cache",
                'Authorization': self._token,
                'content-type': contentType,
                'Connection':"keep-alive"
                # 'X-Content-Type-Options':"nosniff"
            }
            response = requests.request(url = url,method=method, headers=headers, verify=False, json=jsonData, data=payload);
            
            if(endpoint == 'login'):
                print(colors.LightMagenta+"logging in..."+colors.Reset);
                self._token = json.loads(response.content)['token']
            
            if(response.status_code == 200):
                print(colors.Green+'retour serveur 200'+colors.Reset);
                if (response.headers['content-type'] == 'application/json'):
                    if(response.content):
                        if(fullreponse == False):
                            return(json.loads(response.content));
                        else:
                            return(response);
                    else:
                        print(colors.LightMagenta+"No response content"+colors.Reset);
                    if(response.request.body):
                        print(colors.LightYellow+"response.request.body => " + response.request.body+colors.Reset);
                        if(fullreponse == False):
                            return(response.request.body);
                        else:
                            return(response);
                    else :
                        print(colors.LightMagenta+"No response request body"+colors.Reset);
                return(response);
            
            if(response.status_code == 204):
                print(colors.Green+'retour serveur 204 (Succes sans contenu)'+colors.Reset);
                return(204);
            
            elif(response.status_code == 403):
                print(colors.LightRed+'retour serveur 403 (Accès interdit)'+colors.Reset);
                return({'error':403});
                
            elif(i<4 and (response.status_code == '503' or response.status_code == '502' or response.status_code == '403')):
                print(colors.LightYellow+'response.status_code='+str(response.status_code)+colors.Reset);
                time.sleep(5);
            else:
                print(colors.LightRed+'callAPI error ('+str(response.status_code)+')'+colors.Reset);
                # pprint(vars(response));
                # print('');
                pprint(response.text);
                # raise RaiseError(response.text);
        return(response.content);


##### Routes #####
    
    def mFilterPost(self, groupName, module, jd):
        response = self.callAPI('mfilter/'+groupName+'/module/'+module, "POST", jsonData=jd);
        return(response);
    
    def betaModulePivotDimension(self, module, pivot, dimension):
        response = self.callAPI('beta/module/'+module+'/pivot/'+pivot+'/dimension/'+dimension, "POST");
        return(response);
    
    def betaModulePivotDimensionDel(self, module, pivot, dimension):
        response = self.callAPI('beta/module/'+module+'/pivot/'+pivot+'/dimension/'+dimension, "DELETE");
        return(response);

    def betaModulePivotMetricPost(self, module, pivot, metric, jd):
        response = self.callAPI('beta/module/'+module+'/pivot/'+pivot+'/metric/'+metric, "POST", jsonData=jd);
    
    def betaModulePivotMetricPut(self, module, pivot, metric, jd):
        response = self.callAPI('beta/module/'+module+'/pivot/'+pivot+'/metric/'+metric, "PUT", jsonData=jd);
    
    def betaModulePivotMetricDel(self, module, pivot, metric):
        response = self.callAPI('beta/module/'+module+'/pivot/'+pivot+'/metric/'+metric, "DELETE");


##### General #####


    # Get information on Opensee version
    def ping(self):
        response = self.callAPI('ping', "GET");
        return(response);


    # Provide credentials to get a token
    def login(self, login, password):
        response = self.callAPI('login',"POST", contentType='application/x-www-form-urlencoded' ,jsonData={'identifier':login, 'password':password});
        return(response);


    # Get user information
    def whoami(self):
        response = self.callAPI('whoami',"GET");
        return(response);


    # Get modules names
    def modules(self):
        response = self.callAPI('modules',"GET");
        return(response);


    # Get all module's elements
    def moduleElements(self, modulename):
        response = self.callAPI('module/'+modulename,"GET");
        return(response);


    # Get module definition
    def moduleDefinition(self, modulename):
        response = self.callAPI('module/'+modulename+'/definition',"GET");
        return(response);


    # List pivots definitions of a module
    def modulePivot(self, modulename):
        response = self.callAPI('module/'+modulename+'/pivot',"GET");
        return(response);


    # Get definition for a given pivot
    def modulePivotDef(self, modulename, pivotname):
        response = self.callAPI('module/'+modulename+'/pivot/'+pivotname,"GET");
        return(response);


    # Get all limits of a given module
    def moduleLimit(self, modulename):
        response = self.callAPI('module/'+modulename+'/limit',"GET");
        return(response);


    # Get a given limit of a given module
    def moduleLimitDef(self, modulename, limit):
        response = self.callAPI('module/'+modulename+'/limit/'+limit,"GET");
        return(response);


    # Get the definition of all dimension groups of a given module
    def moduleDimension(self, modulename):
        response = self.callAPI('module/'+modulename+'/dimension/group',"GET");
        return(response)


    # Get the definition of a given dimension group of a module
    def moduleDimensionGroup(self, modulename, groupname):
        response = self.callAPI('module/'+modulename+'/dimension/group/'+groupname,"GET");
        return(response)


    # Provide the full schema for a given table
    def tablesSchema(self, tablename):
        response = self.callAPI('tables/'+tablename+'/schema',"GET");
        return(response)


    # Opensee settings for a given table
    def tablesSettings(self, tablename):
        response = self.callAPI('tables/'+tablename+'/settings',"GET");
        return(response)


    # Opensee ingestion settings for a given table
    def tableIngestion(self, modulename, tablename):
        response = self.callAPI('module/'+modulename+'/tables/'+tablename+'/ingestion_settings',"GET");
        return(response)



##### Users #####

    # create a user
    def userCreate(self, jd):
        response = self.callAPI('users',"POST", jsonData=jd);
        return(response);

    # list all existing users
    def userList(self):
        response = self.callAPI('users',"GET");
        return(response);

    # get an user
    def userId(self, login):
        response = self.callAPI('users/'+login,"GET");
        return(response);

    # delete an user
    def userDelete(self, login):
        response = self.callAPI('users/'+login,"DELETE");
        return(response);

    # reset the password of an user
    def userReset(self, login, password):
        jd = {"identifier": login, "password": password}
        response = self.callAPI('users/'+login+'/resetpassword',"POST", jsonData=jd);
        return(response);

    # list all the groups of one user
    def userListGroup(self, login):
        response = self.callAPI('user/'+login+'/group',"GET");
        return(response);


##### Groups #####

    # list all the groups
    def groupList(self):
        response = self.callAPI('group',"GET");
        return(response);

    # create a new empty group just for the right policy
    def groupCreate(self, groupname):
        response = self.callAPI('group/'+groupname,"POST");
        return(response);

    # get one group
    def groupGet(self, groupname):
        response = self.callAPI('group/'+groupname,"GET");
        return(response);

    # create or update a database user group
    def groupDbUser(self, groupname, dbUser):
        jd = {"groupName": groupname, "dbUser":dbUser}
        response = self.callAPI('dbuser/'+groupname+"/"+dbUser,"POST", jsonData=jd);
        return(response);

    # create or update a mandatory filter user group
    def groupfilter(self, groupname, jd):
        response = self.callAPI('filter/'+groupname,"POST", jsonData=jd);
        return(response);

    # add a policy to a group.
    def groupPolicyadd(self, groupname, jd):
        response = self.callAPI('group/'+groupname+"/policy","PUT", jsonData=jd);
        return(response);

    # remove a policy from a group
    def groupPolicydel(self, groupname, jd):
        response = self.callAPI('group/'+groupname+"/policy","DELETE", jsonData=jd);
        return(response);

    # delete a group
    def groupDelete(self, groupname):
        response = self.callAPI('group/'+groupname,"DELETE");
        return(response);

    # list all users in a group
    def groupUserList(self, groupname):
        response = self.callAPI('group/'+groupname+'/user',"GET");
        return(response);

    # add a user to a group
    def groupUseradd(self, groupname, user):
        jd = {"groupName": groupname, "userId":user}
        response = self.callAPI('group/'+groupname+"/user/"+user,"PUT", jsonData=jd);
        return(response);

    # add a user to multiple groups
    def groupUseraddmult(self, groupname, user):
        response = self.callAPI('group/user/'+user,"PUT", jsonData=groupname);
        return(response);

    # remove a user from a group
    def groupUserremove(self, groupname, user):
        jd = {"groupName": groupname, "userId":user}
        response = self.callAPI('group/'+groupname+"/user/"+user,"DELETE", jsonData=jd);
        return(response);


##### Data Accessibility #####

    # Get values of all enumerable dimensions
    def dataGetdim(self, modulename):
        response = self.callAPI('module/'+modulename+'/values',"GET");
        return(response);

    # Get values of a given dimension
    def dataGetdimValue(self, modulename, dimensionname):
        response = self.callAPI('module/'+modulename+'/values/'+dimensionname,"GET");
        return(response);

    # Smart filter on a given dimension
    def dataGetdimFilter(self, modulename, jd):
        response = self.callAPI('module/'+modulename+'/filters',"POST", jsonData=jd);
        return(response);

    # Query a pivot
    def dataQueryPivot(self, modulename, sessionId, data):
        jd = {'module':modulename};
        response = self.callAPI('module/'+modulename+'/query',"POST", jsonData=data);
        return(response);
    
    # Query most granular data
    def dataGetDetails(self, modulename, data):
        jd = {'module':modulename};
        response = self.callAPI('module/'+modulename+'/details',"POST", jsonData=data);
        return(response);
    
    # Get all calculator definitions for a given module
    def dataGetCalculator(self, modulename):
        response = self.callAPI('module/'+modulename+'/calculator',"GET");
        return(response);

    # Create or update a given calculator in a given module
    def dataCreateCalculator(self, modulename, calculatorname, data):
        response = self.callAPI('module/'+modulename+'/calculator/'+calculatorname,"POST", jsonData=data);
        return(response);
    
    # Get the definition of a given calculator for a given module
    def dataGetCalculatorDef(self, modulename, calculatorname):
        response = self.callAPI('module/'+modulename+'/calculator/'+calculatorname,"GET");
        return(response);

    # Delete a given calculator in a given module
    def dataDelCalculator(self, modulename, calculatorname, data):
        response = self.callAPI('module/'+modulename+'/calculator/'+calculatorname,"DELETE", jsonData=data);
        return(response);

    # Get personalized drill functions for a given calculator in a given module
    def dataGetCalculatorUserdrill(self, modulename, calculatorname):
        response = self.callAPI('module/'+modulename+'/calculator/'+calculatorname+'/listUserDrill',"GET");
        return(response);
    
    # Run a Python calculator
    def dataRunPython(self, modulename, data):
        response = self.callAPI('module/'+modulename+'/calculate',"POST", jsonData=data);
        return(response);
    
    # Run a given calculator request
    def dataCalculatorRequest(self, modulename, data):
        response = self.callAPI('module/'+modulename+'/calculatorrequest',"POST", jsonData=data);
        return(response);

    # Test the calculator on the server without saving it
    def dataTestCalculator(self, modulename, data):
        response = self.callAPI('module/'+modulename+'/testcalculator',"POST", jsonData=data);
        return(response);

    # Run a given calculator request
    def dataCalculatorRequest(self, modulename, data):
        response = self.callAPI('module/'+modulename+'/calculatorrequest',"POST", jsonData=data);
        return(response);
    
    


##### Versioning #####
    
    # Create a commit (update, insert or light delete)
    def versionCommit(self, tablename, jd):
        response = self.callAPI('tables/'+tablename+'/commits',"POST", jsonData=jd);
        return(response);
    
    # Get all commits for a given Date
    def versionCommitDate(self, tablename, date):
        response = self.callAPI('tables/'+tablename+'/commits/'+date,"GET");
        return(response);

    # Provide the schema of a given table
    def versionCommitDef(self, tablename):
        response = self.callAPI('tables/'+tablename+'/commits',"OPTIONS");
        return(response);

    # Get all ancestor commits of a given commit on a given date
    def versionAncestorCommitDate(self, tablename, commit, asof):
        response = self.callAPI('tables/'+tablename+'/commits/'+commit+'/'+asof,"GET");
        return(response);
    
    # Get branches for the requested table
    def versionBranches(self, tablename):
        response = self.callAPI('tables/'+tablename+'/branches',"GET");
        return(response);

    # Get commits of a branch for a given date
    def versionCommitDefDate(self, tablename, branchname, asof):
        response = self.callAPI('tables/'+tablename+'/branches/'+branchname+'/'+asof,"GET");
        return(response);
    
    # Change a comment for given commit
    def versionCommitComment(self, tablename, commit, asof, jd):
        response = self.callAPI('tables/'+tablename+'/commits/'+commit+'/asOf/'+asof,"PATCH", jsonData=jd);
        return(response);



##### Data ingestion #####
    
    # Delete a specific commit
    def ingestionDelCommit(self, tablename, commit, asOf, jd):
        response = self.callAPI('tables/'+tablename+'/commits/'+commit+'/'+asOf,"DELETE", jsonData=jd);
        return(response);
    
    # Insert data in a dictionary, generating a private key for each row.
    def dataInsert(self, asof, dictname, body):
        jd = {};
        response = self.callAPI('dictionaries/'+dictname+"/"+asof,"POST", contentType="text/csv" ,jsonData=jd);
        return(response);
    
    # Start a new transaction
    def ingestionTransactionOpen(self, tablename, asof, sourceBranch, comment):                
        response = self.callAPI('tables/'+tablename+'/transaction/'+asof+'/from/'+sourceBranch+'/'+comment,"POST", contentType="text/csv");
        return(response.text);

    def ingestionFactTable(self, table, transaction_id, asof, file_full_path):
        with open(file_full_path, 'rb') as fp:
            response = self.callAPI('tables/'+table+'/commits/'+transaction_id+'/'+asof,"POST", contentType="text/csv", payload=fp);
        return(response);

    def ingestionRollback(self, table, transaction_id, asof):
        response = self.callAPI('tables/'+table+'/transaction/'+transaction_id+'/'+asof,"DELETE", contentType="text/csv");
        return(response);
    
    # Close an open transaction
    def ingestionTransactionClose(self, tablename, transaction_id, asof, destinationBranch):
        response = self.callAPI('tables/'+tablename+'/closeTransaction/'+transaction_id+'/'+asof+'/to/'+destinationBranch, "POST", contentType="text/csv");
        return(response);

    # Retrieve the number of rows inserted
    def ingestionRetriew(self, tablename, asOf, transaction_id):
        response = self.callAPI('tables/'+tablename+'/progressTransaction/commits/'+transaction_id+'/'+asOf, "POST");
        return(response);


##### Configuration #####

    
    def configDefGet(self, modulename):
        response = self.callAPI('module/'+modulename+'/definition',"GET");
        return(response);

    # Update a module definition
    def configDefPost(self, modulename, jd):
        response = self.callAPI('module/'+modulename+'/definition',"POST", jsonData=jd);
        return(response);
    
    def configPivotGet(self, modulename, pivotName):
        response = self.callAPI('module/'+modulename+'/pivot/'+pivotName,"GET");
        return(response);   
    
    # Update pivot of a given module
    def configPivotPost(self, modulename, pivotName, jd):
        response = self.callAPI('module/'+modulename+'/pivot/'+pivotName,"POST", jsonData=jd);
        return(response);

    def configLimitGet(self, modulename, limit):
        response = self.callAPI('module/'+modulename+'/limit/'+limit,"GET");
        return(response);
    
    # Create or update a limit of a module
    def configLimitPost(self, modulename, limit, jd):
        response = self.callAPI('module/'+modulename+'/limit/'+limit,"POST", jsonData=jd);
        return(response);

    # Delete a limit in a given module
    def configLimitDelete(self, modulename, limit, jd):
        response = self.callAPI('module/'+modulename+'/limit/'+limit,"DELETE", jsonData=jd);
        return(response);

    def configIngestGet(self, modulename, tablename):
        response = self.callAPI('module/'+modulename+'/tables/'+tablename+'/ingestion_settings',"GET");
        return(response);

    # Insert ingestion settings for a given table
    def configIngestPost(self, modulename, tablename, jd):
        response = self.callAPI('module/'+modulename+'/tables/'+tablename+'/ingestion_settings',"POST", jsonData=jd);
        return(response);
    
    # Delete ingestion settings for a given table
    def configIngestDel(self, modulename, tablename, jd):
        response = self.callAPI('module/'+modulename+'/tables/'+tablename+'/ingestion_settings',"DELETE", jsonData=jd);
        return(response);
    
    # Add a dimension to a module
    def configDimPost(self, modulename, tablename, jd):
        response = self.callAPI('module/'+modulename+'/table/'+tablename,"POST", jsonData=jd);
        return(response);
    
    # Delete a dimension from a module
    def configDimDel(self, modulename, tablename, jd):
        response = self.callAPI('module/'+modulename+'/table/'+tablename,"DELETE", jsonData=jd);
        return(response);

    # Add a dimension to a module
    def configDimPost2(self, modulename, database, tablename, jd):
        response = self.callAPI('module/'+modulename+'/database/'+database+'/table/'+tablename,"POST", jsonData=jd);
        return(response)

    # Delete a dimension from a module
    def configDimDel2(self, modulename, database, tablename, jd):
        response = self.callAPI('module/'+modulename+'/database/'+database+'/table/'+tablename,"DELETE", jsonData=jd);
        return(response)

##### Dot Net Configuration #####

    # Get all dot net configurations for a given type
    def dnetInfo(self, typename):
        response = self.callAPI('dotnetconfig/'+typename,"GET");
        return(response);
    
    # Update or create a dot net configuration
    def dnetCreate(self, jd):
        response = self.callAPI('dotnetconfig',"POST", jsonData=jd);
        return(response);
    
    # Delete a dot net configuration for a given type and a given object
    def dnetDel(self, name, typename):
        response = self.callAPI('dotnetconfig/'+typename+'/'+name,"DELETE");
        return(response);
    


##### AbstractionLayer #####
    
    # Get informations about joins available for this module
    def alGetJoin(self, module):
        response = self.callAPI('module/'+module+'/joins',"GET");
        return(response);
    
    # meta is a new endpoint which will be used by users or frontends to get all the needed information to then perform the request through the abstraction API.
    def alMeta(self, module):
        response = self.callAPI('module/'+module+'/meta',"GET");
        return(response);
    
    # Retrieve all available branches (and its dates) of a module
    def alBranches(self, module):
        response = self.callAPI('module/'+module+'/branches',"GET");
        return(response);
    
    # This endpoint is to get a list of pivot, that will then be able to use to run the existing endpoints smart filters (or values).
    def alPivot(self, module, jd):
        response = self.callAPI('module/'+module+'/pivotSources',"POST", jsonData=jd);
        return(response);
    
    # This endpoint is to get a list of pivot, that will then be able to use to run the existing endpoints smart filters (or values).
    # def alPivotSource(self, module, jd):
        # response = self.callAPI('module/'+module+'/pivotSources',"POST", jsonData=jd);
        # return(response);
    
    # This endpoint allows executing a source (pivot or calculator or join) with at least given dimensions, metrics and filter.
    def alRequest(self, module, jd):
        response = self.callAPI('module/'+module+'/request',"POST", jsonData=jd);
        return(response);
    
    # This endpoint explains the execution of /request endpoint with a input. AbstractionLayer will execute the first correspondent source which matches all requirements (dimensions, metrics)
    def alExplain(self, module, jd):
        response = self.callAPI('module/'+module+'/explain',"POST", jsonData=jd);
        return(response);
        
        
        
    
##### Cache #####

    # Reload a specific dictionary.
    def cacheReload(self, dictionary):
        response = self.callAPI('dictionary/'+dictionary+'/reload',"POST");
        return(response);



##### Batch #####

    # returns all batch status on given module
    def batchStatus(self, module):
        response = self.callAPI('module/'+module+'/status',"GET");
        return(response);

    # Removes old stored batch data
    def batchClean(self, jd):
        response = self.callAPI('batch/clean',"POST", jsonData=jd);
        return(response);

    # Removes only one batch data
    def batchDel(self, bid):
        response = self.callAPI('batch/'+bid,"DELETE");
        return(response);


##### AutoCube #####
    
    # Creates a Opensee cube from a datafile
    def autocubeCreate(self, module, jsonFile="autocube_input.json", mode="Create", jd=""):
        if (jd == ""):
            with open(jsonFile, "r") as f:
                jd = json.load(f);
        jd['mode'] = mode;
        
        response = self.callAPI('autocube/'+module, "POST", contentType="json", jsonData=jd);
        return(response);


##### Adjustment validation workflows #####

    # Adds a new validationflow
    def workflowNew(self, jd):
        response = self.callAPI('validation/flow', "POST", contentType="json", jsonData=jd);
        return(response);
    
    # Lists all validationflows
    def workflowList(self):
        response = self.callAPI('validation/flow', "GET");
        return(response);
    
    # Retrievs an existing validationflow
    def workflowRetriev(self, name):
        response = self.callAPI('validation/flow/'+name, "GET");
        return(response);
    
    # Create a new validation request based on a validation flow.
    def workflowCreateValidation(self, jd):
        response = self.callAPI('validation/request', "POST", contentType="json", jsonData=jd);
        return(response);
    
    # Approve one step of a validation request
    def workflowAccept(self, requestId, name):
        response = self.callAPI('validation/request/'+str(requestId)+'/state/'+name+'/accept', "POST");
        return(response);
        
    # Reject one step of a validation request
    def workflowReject(self, requestId, name):
        response = self.callAPI('validation/request/'+str(requestId)+'/state/'+name+'/reject', "POST");
        return(response);
    
    # Retry the last step of a validation request
    def workflowRetry(self, requestId, name):
        response = self.callAPI('validation/request/'+str(requestId)+'/state/'+name+'/retry', "POST");
        return(response);
        
    # Close a validation request
    def workflowClose(self, requestId):
        response = self.callAPI('validation/request/'+str(requestId)+'/close', "POST");
        return(response);
    
    # Reopen a closed validation request
    def workflowReopen(self, requestId):
        response = self.callAPI('validation/request/'+str(requestId)+'/reopen', "POST");
        return(response);
        
    # Get detail of validation request (name, table, states,...)
    def workflowDetails(self, requestId):
        response = self.callAPI('validation/request/'+str(requestId), "GET");
        return(response);